import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgxEchartsModule } from 'ngx-echarts'

import { AppComponent } from './app.component';
import { SimpleGraphComponent } from './simple-graph/simple-graph.component';
import { ZoomGraphComponent } from './zoom-graph/zoom-graph.component';
import { DraggingGraphComponent } from './dragging-graph/dragging-graph.component';
import { RealTimeComponent } from './real-time/real-time.component';

@NgModule({
  declarations: [
    AppComponent,
    SimpleGraphComponent,
    ZoomGraphComponent,
    DraggingGraphComponent,
    RealTimeComponent
  ],
  imports: [
    BrowserModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
