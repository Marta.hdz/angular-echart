import { Component, OnInit } from '@angular/core';
import { EChartsOption } from 'echarts';
import { realTime } from 'src/data/realTime';

@Component({
  selector: 'app-real-time',
  templateUrl: './real-time.component.html',
  styleUrls: ['./real-time.component.scss']
})
export class RealTimeComponent implements OnInit {

  data: number[][] = realTime
  options: EChartsOption = {}
  symbolSize: number = 10
  updatedOptions: EChartsOption = {}

  constructor() {}

  ngOnInit() {
    this.setOptions()
    this.realTimeMockData()
  }
  
  realTimeMockData(): void{
    setInterval(() => {
      const previousTime: Date = new Date(this.data[this.data.length - 1][0])
      const oneDay: number = 1000 * 60 * 60 * 24 
      const nextTime: Date = new Date(previousTime.getTime() + oneDay)
      this.data.push([+nextTime, Math.random() + 1])
      this.updateGraph()
    }, 1000)
  }

  updateGraph(): void {
    this.updatedOptions = { 
      series: [{
        data: this.data
      }]
    }
  }

  setOptions(): void {
    this.options = {
      color: "#00838F",
      grid: {
        left: '0%',
        right: '4%',
        bottom: '15%',
        containLabel: true
      },
      xAxis: {
        type: 'time',
        splitLine: {
            show: false
        }
      },
      yAxis: {
        min: 0,
        max: 3,
        type: 'value'
      },
      series: [
        {
          id: 'a',
          type: 'line',
          smooth: true,
          showSymbol: false,
          data: this.data
        }
      ]
    }
  }
}