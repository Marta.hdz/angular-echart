import { Component, OnInit } from '@angular/core';
import { EChartsOption } from 'echarts';
import * as util from 'zrender/lib/core/util';
import { dragPoints } from '../../data/dragPoints';

@Component({
  selector: 'app-dragging-graph',
  templateUrl: './dragging-graph.component.html',
  styleUrls: ['./dragging-graph.component.scss']
})
export class DraggingGraphComponent implements OnInit {
  
  data: number[][] = dragPoints
  options: EChartsOption = {}
  symbolSize: number = 10

  constructor() {}

  ngOnInit() {
    this.setOptions()
  }
  
  onChartReady(myChart: any): void {
    const data = this.data

    const onPointDragging = function(this: any, dataIndex: any) {
      const newPoint = myChart.convertFromPixel({ gridIndex: 0 }, this.position) as number[]
      data[dataIndex] = newPoint
      // data[dataIndex][1] = newPoint[1]

      myChart.setOption({
        series: [
          {
            id: 'a',
            data: data,
          }
        ]
      })

      // this.parentComponent.emit(data)
    }

    setTimeout(() => {
      myChart.setOption({
        graphic: util.map(data, (item, dataIndex) => {
          return {
            type: 'circle',
            position: myChart.convertToPixel({ gridIndex: 0 }, item),
            shape: {
              cx: 0,
              cy: 0,
              r: this.symbolSize / 2
            },
            invisible: true,
            draggable: true,
            ondrag: util.curry<(dataIndex: any) => void, number>(onPointDragging, dataIndex!),
            z: 100
          }
        }),
      })
    }, 0)
  }

  setOptions(): void {
    this.options = {
      color: "#00838F",
      tooltip: {
        triggerOn: 'mousemove',
        formatter:(params: any) => {
          return 'X: ' + params.data[0] + '<br>Y: ' + params.data[1]
        }
      },
      grid: {
        left: '0%',
        right: '4%',
        bottom: '15%',
        containLabel: true
      },
      xAxis: {
        min: 0,
        max: 100,
        type: 'value'
      },
      yAxis: {
        min: 0,
        max: 100,
        type: 'value'
      },
      dataZoom: [
        {
          type: 'slider',
          xAxisIndex: 0
        },
        {
          type: 'slider',
          yAxisIndex: 0
        },
        {
          type: 'inside',
          xAxisIndex: 0
        },
        {
          type: 'inside',
          yAxisIndex: 0
        },
      ],
      series: [
        {
          id: 'a',
          type: 'line',
          smooth: true,
          symbolSize: this.symbolSize,
          data: this.data
        }
      ]
    }
  }

}
