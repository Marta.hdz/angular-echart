import { Component, OnInit } from '@angular/core';
import { EChartsOption } from 'echarts';

@Component({
  selector: 'app-simple-graph',
  templateUrl: './simple-graph.component.html',
  styleUrls: ['./simple-graph.component.scss']
})
export class SimpleGraphComponent implements OnInit {

  updatePosition: (() => void) | undefined
  data: number[][] = [
    [8.07, 6.95],
    [9.05, 8.81],
    [9.15, 7.20],
    [3.03, 4.23],
    [2.02, 4.47],
    [1.05, 3.33],
    [4.05, 4.96],
    [6.03, 7.24],
    [7.08, 5.82],
    [5.02, 5.68]
  ]

  options: EChartsOption = {}
  symbolSize: number = 10

  constructor() {}

  ngOnInit() {
    this.setOptions()
  }

  setOptions(): void {
    this.options = {
      xAxis: {
        min: 0,
        max: 15,
        type: 'value'
      },
      yAxis: {
        min: 0,
        max: 15,
        type: 'value'
      },
      series: [
        {
          id: 'simple graph',
          type: 'scatter',
          symbolSize: this.symbolSize,
          data: this.data
        }
      ]
    }
  }
}