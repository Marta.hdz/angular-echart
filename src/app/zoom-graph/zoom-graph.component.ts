import { Component, OnInit } from '@angular/core';
import { EChartsOption } from 'echarts';
import { graphPoints } from '../../data/graphPoints';

@Component({
  selector: 'app-zoom-graph',
  templateUrl: './zoom-graph.component.html',
  styleUrls: ['./zoom-graph.component.scss']
})
export class ZoomGraphComponent implements OnInit {

  
  updatePosition: (() => void) | undefined
  data: number[][] = graphPoints
  options: EChartsOption = {}
  symbolSize: number = 10

  constructor() {}

  ngOnInit() {
    this.setOptions()
  }

  setOptions(): void {
    this.options = {
      tooltip: {
        triggerOn: 'mousemove',
        formatter:(params: any) => {
          return 'X: ' + params.data[0] + '<br>Y: ' + params.data[1]
        }
      },
      grid: {
        left: '0%',
        right: '4%',
        bottom: '15%',
        containLabel: true
      },
      xAxis: {
        min: 0,
        max: 15,
        type: 'value',
        axisLabel: {
          formatter: '{value} °C'
        }
      },
      yAxis: {
        min: 0,
        max: 15,
        type: 'value'
      },
      dataZoom: [
        {
          type: 'slider',
          xAxisIndex: 0
        },
        {
          type: 'slider',
          yAxisIndex: 0
        },
        {
          type: 'inside',
          xAxisIndex: 0
        },
        {
          type: 'inside',
          yAxisIndex: 0
        }
      ],
      series: [
        {
          id: 'a',
          type: 'scatter',
          symbolSize: this.symbolSize,
          symbol: 'triangle',
          data: this.data,
          itemStyle: {
            borderWidth: 3,
            borderColor: '#EE6666',
            color: 'yellow'
          }
        }
      ]
    }
  }
}