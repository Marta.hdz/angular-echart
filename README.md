# AngularEchart

This project was was created to shows Echarts from Apache with Angular. It was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.7.

## Development server

- To start run `docker-compose up --build`
The app is available on `http://localhost:4300/`

## Documentation
- [ngx-echarts](https://xieziyu.github.io/ngx-echarts/#/welcome)
- [echarts](https://echarts.apache.org/examples/en/index.html)
